package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
)

type instruction struct {
  count int
  start int
  end int
}

const (
  stacksInput = "stacks"
  stepsInput = "steps"
)

func importStacks() {

}

func importSteps(file string) (InstructionSet []instruction) {
  filepointer, err := os.Open(file)
  if err != nil {
    fmt.Printf("Error reading file: %v\n", err)
    os.Exit(1)
  }
  defer filepointer.Close()
  numbers := regexp.MustCompile(`\d+`)
  scanner := bufio.NewScanner(filepointer)
  var theInstruction instruction
  var err1 error
  var err2 error
  var err3 error
  for scanner.Scan() {
    matches := numbers.FindAllString(scanner.Text(), -1)
    theInstruction.count, err1 = strconv.Atoi(matches[0])
    theInstruction.start, err2 = strconv.Atoi(matches[1])
    theInstruction.end, err3 = strconv.Atoi(matches[2])
    if err1 != nil || err2 != nil || err3 != nil {
      fmt.Println("Error converting strings to int")
      fmt.Printf("Error 1: %v\n", err1)
      fmt.Printf("Error 2: %v\n", err2)
      fmt.Printf("Error 3: %v\n", err3)
      os.Exit(1)
    }
    InstructionSet = append(InstructionSet, theInstruction)
  }
  return InstructionSet
}

func executeinstruction(stacks [][]rune, theInstruction instruction) (newstacks [][]rune) {
  oglen := len(stacks[theInstruction.start])
  for i := 1; i <= theInstruction.count && i < oglen; i++ {
    fmt.Printf("i: %v\tCount: %v\tStart: %v\tEnd: %v\n", i, theInstruction.count, theInstruction.start, theInstruction.end)
    theRune := stacks[theInstruction.start][len(stacks[theInstruction.start])-1]
    stacks[theInstruction.end] = append(stacks[theInstruction.end], theRune)
  }
  return newstacks
}

func printStacks(stacks [][]rune) {
  totalelements := 0
  for i := 0; i < len(stacks); i++ {
    fmt.Printf("Stack %v(%v): ", i+1, len(stacks[i]))
    totalelements += len(stacks[i])
    for j := 0; j < len(stacks[i]); j++ {
      fmt.Printf("[%c] ", stacks[i][j])
    }
    fmt.Printf("\n")
  }
  fmt.Printf("Total Elements %v\n", totalelements)
}

func Part1(stacks [][]rune, TheInstructions []instruction) {
  fmt.Println("Part 1")
  for _, instruct := range TheInstructions {
    //fmt.Printf("Count: %v\tStart: %v\tEnd: %v\n", instruct.count, instruct.start, instruct.end)
    //stacks = executeinstruction(stacks, instruct)
    oglen := len(stacks[instruct.start-1])
    for i := 1; i <= instruct.count && i <= oglen; i++ {
      //fmt.Printf("i: %v\tCount: %v\tStart: %v\tEnd: %v\n", i, instruct.count, instruct.start, instruct.end)
      theRune := stacks[instruct.start-1][len(stacks[instruct.start-1])-1]
      stacks[instruct.end-1] = append(stacks[instruct.end-1], theRune)
      stacks[instruct.start-1] = stacks[instruct.start-1][:len(stacks[instruct.start-1])-1]
    }
    //printStacks(stacks)
  }
  printStacks(stacks)
}

func Part2(stacks [][]rune, TheInstructions []instruction) {
  fmt.Println("Part 2")
  for _, instruct := range TheInstructions {
    fmt.Printf("Count: %v\tStart: %v\tEnd: %v\n", instruct.count, instruct.start, instruct.end)
    oglen := len(stacks[instruct.start - 1])
    if oglen > 0 {
      var MoveCount int
      if oglen < instruct.count {
        MoveCount = oglen
      } else {
        MoveCount = instruct.count
      }
      fmt.Printf("Starting Slices %v\n", MoveCount)
      tempSlice := stacks[instruct.start - 1][len(stacks[instruct.start - 1]) - MoveCount:]
      stacks[instruct.end - 1] = append(stacks[instruct.end - 1], tempSlice...)
      fmt.Println("First Slicing complete")
      stacks[instruct.start - 1] = stacks[instruct.start-1][:len(stacks[instruct.start - 1]) - MoveCount]
      fmt.Println("Second Slicing complete")

      printStacks(stacks)
    }
  }
  //printStacks(stacks)
}

func main() {
  fmt.Println("Entering Main")
  stacks := [][]rune {
    {'F', 'T', 'C', 'L', 'R', 'P', 'G', 'Q'},
    {'N', 'Q', 'H', 'W', 'R', 'F', 'S', 'J'},
    {'F', 'B', 'H', 'W', 'P', 'M', 'Q'},
    {'V', 'S', 'T', 'D', 'E'},
    {'Q', 'L', 'D', 'W', 'V', 'F', 'Z'},
    {'Z', 'C', 'L', 'S'},
    {'Z', 'B', 'M', 'V', 'D', 'F'},
    {'T', 'J', 'B'},
    {'Q', 'N', 'B', 'G', 'L', 'S', 'P', 'H'},
  }
  //fmt.Printf("%v\n", stacks)
  printStacks(stacks)
  TheInstructions := importSteps(stepsInput)
  //Part1(stacks, TheInstructions)
  Part2(stacks, TheInstructions)
}
