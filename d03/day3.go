package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

// Constants
const InputFile string = "input"

func readFileContents(filename string) (fileContentsArray []string, Err error) {
	fmt.Println("Getting file contents")
	file, err := os.Open(filename)
	if err != nil {
		fmt.Println("Error opening File: ", err)
		return fileContentsArray, err
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	var contents string
	for scanner.Scan() {
		line := scanner.Text()
		contents += line + "\n"
	}
	if err := scanner.Err(); err != nil {
		return fileContentsArray, Err
	}
	fileContentsArray = strings.Split(contents, "\n")
	fileContentsArray = fileContentsArray[:len(fileContentsArray)-1]
	return fileContentsArray, nil
}

func identifyDuplicate(packContents string) (dup rune) {
	//fmt.Printf("Pack Contents: %v\n", packContents)
	midpoint := len(packContents) / 2
	compartment1 := packContents[:midpoint]
	compartment2 := packContents[midpoint:]
	dupIndex := strings.IndexAny(compartment1, compartment2)
	//fmt.Printf("DupIndex: %v\n", dupIndex)
	dup = rune(compartment1[dupIndex])
	//fmt.Printf("Dup: %v\n", dup)
	return dup
}

func identifyBadge(Elf1, Elf2, Elf3 string) (dup rune) {
	foundRune := false
	for foundRune != true || len(Elf1) == 0 {
		dupIndex := strings.IndexAny(Elf1, Elf2)
		dup = rune(Elf1[dupIndex])
		foundRune = strings.ContainsRune(Elf3, dup)
		if !foundRune {
			SubStrings := strings.SplitAfterN(Elf1, string(dup), 2)
			Elf1 = SubStrings[1]
		}
	}
	return dup
}

func main() {
	fmt.Println("Starting Main")
	PriorityMatrix := map[rune]int{
		'a': 1,
		'b': 2,
		'c': 3,
		'd': 4,
		'e': 5,
		'f': 6,
		'g': 7,
		'h': 8,
		'i': 9,
		'j': 10,
		'k': 11,
		'l': 12,
		'm': 13,
		'n': 14,
		'o': 15,
		'p': 16,
		'q': 17,
		'r': 18,
		's': 19,
		't': 20,
		'u': 21,
		'v': 22,
		'w': 23,
		'x': 24,
		'y': 25,
		'z': 26,
		'A': 27,
		'B': 28,
		'C': 29,
		'D': 30,
		'E': 31,
		'F': 32,
		'G': 33,
		'H': 34,
		'I': 35,
		'J': 36,
		'K': 37,
		'L': 38,
		'M': 39,
		'N': 40,
		'O': 41,
		'P': 42,
		'Q': 43,
		'R': 44,
		'S': 45,
		'T': 46,
		'U': 47,
		'V': 48,
		'W': 49,
		'X': 50,
		'Y': 51,
		'Z': 52,
	}
	Output, Err := readFileContents(InputFile)
	if Err != nil {
		fmt.Println("Error returned: ", Err)
		return
	}
	PrioritySum := 0
	for _, line := range Output {
		dupRune := identifyDuplicate(line)
		PrioritySum += PriorityMatrix[dupRune]
	}
	fmt.Printf("The total for Part 1 is: %v\n", PrioritySum)
	PrioritySum = 0
	for i := 0; i < len(Output); i += 3 {
		dupRune := identifyBadge(Output[i], Output[i+1], Output[i+2])
		PrioritySum += PriorityMatrix[dupRune]
	}
	fmt.Printf("The total for Part 2 is: %v\n", PrioritySum)
}
