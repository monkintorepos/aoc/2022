package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

// Constants
const InputFile string = "input"

func readFileContents(filename string) (fileContentsArray []string, Err error) {
	fmt.Println("Getting file contents")
	file, err := os.Open(filename)
	if err != nil {
		fmt.Println("Error opening File: ", err)
		return fileContentsArray, err
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	var contents string
	for scanner.Scan() {
		line := scanner.Text()
		contents += line + "\n"
	}
	if err := scanner.Err(); err != nil {
		return fileContentsArray, Err
	}
	fileContentsArray = strings.Split(contents, "\n")
	fileContentsArray = fileContentsArray[:len(fileContentsArray)-1]
	return fileContentsArray, nil
}

func main() {
	fmt.Println("Starting Main")
	Output, Err := readFileContents(InputFile)
	if Err != nil {
		fmt.Println("Error returned: ", Err)
		return
	}

}
