package main

import(
  "fmt"
  "os"
  "bufio"
  "strings"
  "strconv"
  "sort"
)

// const InputFile string = "input"

func readFileContents(filename string) (string, error) {
  fmt.Println("Getting file contents")
  file, err := os.Open(filename)
  if err != nil {
    fmt.Println("Error opening File: ", err)
    return "", err
  }
  defer file.Close()
  
  scanner := bufio.NewScanner(file)
  
  var contents string
  for scanner.Scan() {
    line := scanner.Text()
    contents += line + "\n"
  }

  if err := scanner.Err(); err != nil {
    return "", err
  }

  return contents, nil
}

func buildArray(FileContents string) ([]int) {
  fmt.Println("Beginning to create the array")
  TheNumbers := strings.Split(FileContents, "\n")
  var TheArray []int
  CurrentElf := 0

  //fmt.Println(TheNumbers)
  //fmt.Println(len(TheNumbers))

  for _, Number := range TheNumbers {
    if len(Number) != 0 {
      Output, Err := strconv.ParseInt(Number, 10, 32) 
      //fmt.Println("Output: ", Output)
      if Err != nil {
        fmt.Println("The Error: ", Err)
        return []int{0,0}
      } else {
        CurrentElf += int(Output)
      }
    } else {
      fmt.Println("Adding Elf with ", CurrentElf, " calories")
      TheArray = append(TheArray, CurrentElf)
      CurrentElf = 0
    }
  }
  //fmt.Println("TheArray: ", TheArray)
  for index, Elf := range TheArray {
    fmt.Println("Elf #", index + 1, " = ", Elf)
  }
  return TheArray
}

//func findMax(TheArray []int) (int) {
//  fmt.Println("Looking for max value")
//  Threshold := 0 
//  var BigElves []int
//  for _, Number := range TheArray {
//    if Number > Threshold {
//      add number to array, sort by value remove the lowest one
//    }
//  }
//  fmt.Println()
//  return MaxSize
//}

func main() {
  fmt.Println("Starting Main")
  Output, Err := readFileContents("input")
  if Err != nil {
    fmt.Println("Error returned: ", Err)
    return 
  }
  TheElves := buildArray(Output)
  //WealthyElf := findMax(TheElves) // Just sort the array and total up the top three elements dummy
  sort.Slice(TheElves, func(i, j int) bool {
    return TheElves[i] > TheElves[j]
  })
  WealthyElf := TheElves[0] + TheElves[1] + TheElves[2]
  fmt.Println("The Elf with the most calories has: ", WealthyElf)
}
