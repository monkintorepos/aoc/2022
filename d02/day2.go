package main

import (
	"fmt"
  "os"
  "bufio"
  "strings"
)
// Translation consts
const OppRock rune = 'A'
const OppPaper rune = 'B'
const OppScissors rune = 'C'
const MeRock rune = 'X'
const MePaper rune = 'Y'
const MeScissors rune = 'Z'
const NeedLose rune = 'X'
const NeedDraw rune = 'Y'
const NeedWin rune = 'Z'

// Scoring
const Rock int = 1
const Paper int = 2
const Scissors int = 3
const Lose int = 0
const Draw int = 3
const Win int = 6

func readFileContents(filename string) (string, error) {
  fmt.Println("Getting file contents")
  file, err := os.Open(filename)
  if err != nil {
    fmt.Println("Error opening File: ", err)
    return "", err
  }
  defer file.Close()
  
  scanner := bufio.NewScanner(file)
  
  var contents string
  for scanner.Scan() {
    line := scanner.Text()
    contents += line + "\n"
  }

  if err := scanner.Err(); err != nil {
    return "", err
  }
  
  return contents, nil
}

func generateArray(Data string) (TheArray [][]rune) {
  TheLines := strings.Split(Data, "\n")
  for _, Line := range TheLines {
    if Line != "" {
      //fmt.Printf("Contents of line \"%v\"\n", Line)
      runes := []rune(Line)
      match := []rune{runes[0], runes[2]}
      //fmt.Printf("contents of match: %v\n", match)
      TheArray = append(TheArray, match)
    }
  }
  return TheArray
}

func CalculateScore(Opp, Me rune) (Score int) {
	switch Me {
	case MeRock:
		Score = Rock
		switch Opp {
		case OppRock:
			Score += Draw
		case OppPaper:
			Score += Lose
		case OppScissors:
			Score += Win
		}
	case MePaper:
		Score = Paper
		switch Opp {
		case OppRock:
			Score += Win
		case OppPaper:
			Score += Draw
		case OppScissors:
			Score += Lose
		}
	case MeScissors:
		Score = Scissors
		switch Opp {
		case OppRock:
			Score += Lose
		case OppPaper:
			Score += Win
		case OppScissors:
			Score += Draw
		}
	}
	return Score
}

func CalculateScore2(Opp, Me rune) (Score int) {
	switch Me {
	  case NeedLose:
      Score = Lose
      switch Opp {
        case OppRock:
          Score += Scissors
        case OppPaper:
          Score += Rock
        case OppScissors:
          Score += Paper
      }
    case NeedDraw:
      Score = Draw
      switch Opp {
        case OppRock:
          Score += Rock
        case OppPaper:
          Score += Paper
        case OppScissors:
          Score += Scissors
      }
    case NeedWin:
      Score = Win
      switch Opp {
        case OppRock:
          Score += Paper
        case OppPaper:
          Score += Scissors
        case OppScissors:
          Score += Rock
      }
  }
	return Score
}

func main() {
  fmt.Println("Beginning Main")
  Contents, Err := readFileContents("input")
  if Err != nil {
    fmt.Printf("Error returned: %v", Err)
    return
  }
  Matches := generateArray(Contents)
  //fmt.Printf("Contents of Matches Array: %v\n", Matches)
  TotalScore := 0
  for _, Match := range Matches {
    TotalScore += CalculateScore2(Match[0], Match[1])
  }
  fmt.Printf("The total score: %v\n", TotalScore)








  //fmt.Printf("The outcome of A v Y is: %v\n", CalculateScore('A', 'Y'))
  //fmt.Printf("The outcome of B v X is: %v\n", CalculateScore('B', 'X'))
  //fmt.Printf("The outcome of C v Z is: %v\n", CalculateScore('C', 'Z'))
}
